# docker-compose-application

Use https://gitlab.com/naturalis/lib/ansible-docker-compose to start and manage your docker-compose application using Ansible.


NOTE: when running for the first time, start by deploying the steward_etl container:

```bash
docker-compose up -d steward_etl
```

and next, start the steward_source container:

```bash
docker-compose up -d steward_source
```

By doing this, you're making sure the necessary directory structure is in place for running the minio containers. You can start the rest now with:

```bash
docker-compose up -d
```

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* [https://github.com/gitleaks/gitleaks] already up to date!
* pre-commit installed at .git/hooks/pre-commit

